const SerialPort = require('serialport')
const Readline = require('@serialport/parser-readline')
const dotenv = require('dotenv')
dotenv.config({ path: './variables.env' })

const port = new SerialPort(`${process.env.SERIALPORT}`, { baudRate: 9600 })
const parser = new Readline({ delimiter: '\r\n' })
port.pipe(parser)

const ipc = require('node-ipc')

ipc.config.id = 'lababoscanner'
ipc.config.retry = 1500
ipc.config.silent = true

port.open((error) => {
  if (error) {
    console.log(error.message)
  }
  console.log('Port is opened.')
  ipc.connectTo('lababoserver', () => {
    console.log('ID scanner is connecting to the server.')
    ipc.of.lababoserver.on('connect', () => {
      console.log('ID scanner has connected to the server.')
      parser.on('data', (data) => {
        console.log('scanned', data)
        ipc.of.lababoserver.emit('scannedid', data)
      })
    })
  })
})
