import { AuthenticationError, ForbiddenError, ValidationError } from 'apollo-server-express'
import users from '../actions/users'
import items from '../actions/items'
import requests from '../actions/requests'

const validateUserLogin = loggedInUser => {
  if (!loggedInUser) {
    throw new AuthenticationError('You are not logged in.')
  }
}
const validateAdmin = loggedInUser => {
  if (!loggedInUser.isAdmin) {
    throw new ForbiddenError('You are not authorized.')
  }
}

export const resolvers = {
  Mutation: {
    advanceRequest: (_, { requestId, status }, { loggedInUser }) => {
      validateUserLogin(loggedInUser)
      validateAdmin(loggedInUser)
      return requests.advance(requestId, status)
    },
    addRequest: async (_, request, { loggedInUser }) => {
      validateUserLogin(loggedInUser)
      return requests.add(loggedInUser.id, request)
    },
    addUser: async (_, user) => {
      if (user.password.length < 8) {
        throw new ValidationError('You password is less than 8 characters.')
      }
      return users.add(user)
    },
    signUser: (_, user) => {
      return users.sign(user)
    },
    signUserById: (_, { rfid }) => {
      return users.signById(rfid)
    },
    saveItem: (_, item, { loggedInUser }) => {
      validateUserLogin(loggedInUser)
      validateAdmin(loggedInUser)
      if (item.id) {
        return items.update(item)
      }
      return items.add(item)
    },
    deleteItem: (_, item, { loggedInUser }) => {
      validateUserLogin(loggedInUser)
      validateAdmin(loggedInUser)
      return items.delete(item)
    }
  },
  Query: {
    requests: (_, { status, from, to }, { loggedInUser }) => {
      validateUserLogin(loggedInUser)
      validateAdmin(loggedInUser)
      return requests.all(status, from, to)
    },
    request: (_, item, { loggedInUser }) => {
      validateUserLogin(loggedInUser)
      validateAdmin(loggedInUser)
      return requests.find(item)
    },
    users: (_) => {
      return users.all()
    },
    user: (_, user, { loggedInUser }) => {
      return users.find(loggedInUser)
    },
    items: (_, { name, from, to }) => {
      return items.all(name, from, to)
    },
  }
}