import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'

export const authorize = async (user, inputPassword) => {
  const correctPassword = await bcrypt.compare(inputPassword, user.password)

  if (!correctPassword) {
    throw new Error('Invalid password')
  }

  const payload = {
    id: user.id,
    studentId: user.studentId,
    isAdmin: user.isAdmin
  }

  return jwt.sign(payload, process.env.SECRET, {
    expiresIn: '2h'
  })
}

export const authorizeById = (user) => {
  const payload = {
    id: user.id,
    studentId: user.studentId,
    isAdmin: user.isAdmin
  }
  return jwt.sign(payload, process.env.SECRET, {
    expiresIn: '2h'
  })
}

export const encryptPassword = async password => {
  const hashes = 7
  const hash = await bcrypt.hash(password, hashes)
  if (!hash) {
    throw new Error('Password hashing error')
  }
  return hash
}

export const authenticate = async authHeader => {
  try {
    const token = extractJwt(authHeader)
    if (token) {
      const verifiedUser = await jwt.verify(token, process.env.SECRET)
      if (!verifiedUser) return undefined
      return jwt.decode(token)
    }
  } catch (e) {
    return undefined
  }
}

export const extractJwt = authHeader => {
  if (authHeader) {
    return authHeader.slice(7)
  }
}
