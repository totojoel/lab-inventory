import Item from '../models/Item'

export default {
  delete: ({ id }) => {
    return Item.findOneAndDelete({ _id: id })
  },
  update: (item) => {
    item.dateUpdated = new Date()
    const { id, ...values } = item
    return Item.findOneAndUpdate({ _id: id }, { ...values })
  },
  add: (item) => {
    item.dateAdded = new Date()
    return new Item(item).save()
  },
  find: (id) => {
    return Item.findById(id)
  },
  all: async (name, from, to) => {
    let filters = {}
    if (name) {
      filters = { name: new RegExp(name, 'i') }
    }

    const items = await Item.find(filters).sort({ dateAdded: -1 })

    return items.map(async (item) => {
      item.availableCount = item.count - (item.requestedCount + item.defectiveCount)
      item.requests = item.requests.filter((request) => {
        if (from && to) {
          return request.dateRequested >= from && request.dateRequested <= to
        }
        if (!from && !to) {
          return true
        }
      })
      item.borrowedCount = 0

      for (const request of item.requests) {
        for (const requestItem of request.items) {
          if (requestItem.item && requestItem.item.id === item.id) {
            item.borrowedCount += requestItem.count
          } 
        }
      }
      return item
    })
  }
}
