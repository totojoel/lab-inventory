import User from '../models/User'
import { authorize, encryptPassword, authorizeById } from '../common/auth'

export default {
  add: async (user) => {
    const payload = {
      ...user,
      password: await encryptPassword(user.password),
    }
    return new User(payload).save()
  },
  sign: async (user) => {
    const { studentId, password } = user
    const result = await User.findOne({ studentId })
    if (!result) {
      throw new Error('Invalid user')
    }
    return authorize(result, password)
  },
  signById: async (rfid) => {
    const result = await User.findOne({ rfid })
    if (!result) {
      throw new Error('Invalid user')
    }
    return authorizeById(result)
  },
  all: async (user) => {
    return User.find(user)
  },
  find: async (user) => {
    return User.findOne({ studentId: user && user.studentId })
  }
}