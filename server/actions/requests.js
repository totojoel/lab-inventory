import Request from '../models/Request'
import Item from '../models/Item'
import User from '../models/User'

export default {
  advance: async (id, status) => {
    const DATES = {
      'ready': 'dateApproved',
      'received': 'dateReceived',
      'returned': 'dateReturned'
    }

    const request = await Request.findOneAndUpdate(
      { _id: id },
      { $set: { status, [DATES[status]]: new Date() } }
    )

    if (status === 'ready') {
      await User.findOneAndUpdate(
        { _id: request.student },
        { $set: { notification: 'Your request is now ready. Please collect your items.' } }
      )
    } else {
      await User.findOneAndUpdate(
        { _id: request.student },
        { $set: { notification: '' } }
      )
    }

    if (status === 'returned') {
      await request.items.forEach(async ({ item, count }) => {
        await Item.findOneAndUpdate({ _id: item.id }, { $inc: { requestedCount: -count } })
      })
    }
    return request
  },
  add: async (studentId, request) => {
    const student = await User.findById(studentId)
    const hasReturned = student.requests.every(request => request.status === 'returned')
    if (!hasReturned) {
      throw new Error('Sorry, please settle your unreturned request.')
    }
    request.student = studentId
    request.dateRequested = new Date()
    const items = request.items.map(({ id, requestCount }) => ({
      item: id,
      count: requestCount
    }))

    const savedRequest = await new Request({ ...request, items }).save()

    await savedRequest.items.forEach(async ({ item, count }) => {
      await Item.findOneAndUpdate(
        { _id: item },
        { $inc: { requestedCount: count }, $push: { requests: savedRequest._id } }
      )
      await User.findOneAndUpdate(
        { _id: studentId },
        { $push: { requests: savedRequest._id } }
      )
    })
    return savedRequest
  }
  ,
  all: (status, from, to) => {
    let filters = {}
    if (status) {
      filters = { status }
    }
    if (from) {
      filters = { ...filters, dateRequested: { $gte: from } }
    }
    if (to) {
      filters = { ...filters, dateRequested: { $gte: from, $lte: to } }
    }
    return Request.find(filters).sort({ dateRequested: -1 })
  },
  findByStudent: (studentId) => {
    return Request.find({ student: studentId })
  },
  find: (item) => {
    return Request.findById(item.id)
  }
}
