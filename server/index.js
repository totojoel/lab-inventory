import express from 'express'
import dotenv from 'dotenv'
dotenv.config({ path: '../variables.env' })
import mongoose from 'mongoose'
import { ApolloServer } from 'apollo-server-express'
import { authenticate } from './common/auth'
import actions from './actions'
import { typeDefs } from './schema'
import { resolvers } from './resolvers'
import fs from 'fs'
import https from 'https'
import http from 'http'

let scannedId = ''

const configurations = {
  production: { ssl: true, port: 4000, hostname: '127.0.1.1' },
  development: { ssl: false, port: 4000, hostname: 'localhost' }
}
const environment = process.env.NODE_ENV || 'production'
const config = configurations[environment]

try {
  mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true, useCreateIndex: true, })
  mongoose.set('useFindAndModify', false);
  console.log('DB CONNECTED')

  const apollo = new ApolloServer({
    typeDefs,
    resolvers: {
      ...resolvers,
      Query: {
        ...resolvers.Query,
        scannedId: () => scannedId,
      }
    },
    context: async ({ req, connection }) => ({
      ...actions,
      loggedInUser: await authenticate(req.headers.authorization),
    })
  })
  const app = express()
  apollo.applyMiddleware({ app })
  
  let server

  if (config.ssl) {
    server = https.createServer(
      {
        key: fs.readFileSync(`./ssl/${environment}/key.pem`, 'utf8'),
        cert: fs.readFileSync(`./ssl/${environment}/server.crt`, 'utf8')
      },
      app
    )
  } else {
    server = http.createServer(app)
  }

  apollo.installSubscriptionHandlers(server)
  server.listen({ port: config.port })

  server.on('listening', () => {
    console.log(`Server ready at http${config.ssl ? 's' : ''}://${config.hostname}:${config.port}${apollo.graphqlPath}`)
    console.log(`Subscriptions ready at ws${config.ssl ? 's' : ''}://${config.hostname}:${config.port}${apollo.graphqlPath}`)
  })
} catch (error) {
  console.log(error)
}

import ipc from 'node-ipc'
import { fork } from 'child_process'
import path from 'path'

const scanner = path.resolve('../scanner.js')
fork(scanner) // make scanner child program

ipc.config.id = 'lababoserver'
ipc.config.retry = 1500
ipc.config.silent = true
ipc.serve(() => {
  ipc.server.on('scannedid', (data) => {
    scannedId = data
    setTimeout(() => scannedId = '', 5000) // expire in 5 seconds
  })
})
ipc.server.start()
