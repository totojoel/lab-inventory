import { gql } from 'apollo-server-express'

export const typeDefs = gql`
  type User {
    id: String,
    studentId: String,
    firstName: String,
    lastName: String,
    isAdmin: Boolean,
    requests: [Request],
    notification: String
  }

  type Item {
    id: String
    barcode: String
    name: String
    count: Int
    requests: [Request]
    requestedCount: Int
    defectiveCount: Int
    availableCount: Int
    borrowedCount: Int
    dateAdded: Float
    dateUpdated: Float
  }

  type RequestItem {
    id: String
    item: Item 
    count: Int
  }

  type Request {
    id: String
    status: String
    student: User
    items: [RequestItem]
    dateRequested: Float
    dateApproved: Float
    dateReceived: Float
    dateReturned: Float
  }

  type Query {
    scannedId: String
    users: [User]
    user: User
    items(name: String, from: Float, to: Float): [Item]
    request(id: String): Request
    requests(status: String, from: Float, to: Float, student: String): [Request]
  }

  input ItemInput {
    id: String!
    requestCount: Int!
  }

  type Mutation {
    advanceRequest(requestId: String!, status: String!): Request
    addRequest(items: [ItemInput]): Request
    addUser(rfid: String, studentId: String!, firstName: String!, lastName: String!, password: String!): User
    signUser(studentId: String!, password: String!): String
    signUserById(rfid: String!): String
    saveItem(id: String, name: String!, count: Int!, barcode: String!, defectiveCount: Int): Item
    deleteItem(id: String!): Item
  }
`
