import mongoose, { Schema } from 'mongoose'
import autopopulate from 'mongoose-autopopulate'

const requestSchema = new Schema({
  status: {
    type: String,
    enum: ['pending', 'ready', 'received', 'returned'],
    default: 'pending',
    required: true
  },
  dateRequested: Number,
  dateApproved: Number,
  dateReceived: Number,
  dateReturned: Number,
  student: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
    autopopulate: true
  },
  items: [{
    item: {
      type: Schema.Types.ObjectId,
      ref: 'Item',
      required: true,
      autopopulate: true
    },
    count: Number
  }]
})

requestSchema.plugin(autopopulate)

module.exports = mongoose.model('Request', requestSchema)
