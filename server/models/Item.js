import mongoose, { Schema } from 'mongoose'
import autopopulate from 'mongoose-autopopulate'

const itemSchema = new Schema({
  barcode: {
    type: String,
    required: true,
    unique: true
  },
  name: {
    type: String,
    required: true
  },
  count: {
    type: Number,
    required: true
  },
  requestedCount: {
    type: Number,
    default: 0
  },
  defectiveCount: {
    type: Number,
    default: 0
  },
  requests: [{
    type: Schema.Types.ObjectId,
    ref: 'Request',
    autopopulate: true,
    required: true
  }],
  dateAdded: {
    type: Number,
    required: true
  },
  dateUpdated: {
    type: Number
  }
})

itemSchema.plugin(autopopulate)

module.exports = mongoose.model('Item', itemSchema)
