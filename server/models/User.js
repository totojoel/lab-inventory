import mongoose, { Schema } from 'mongoose'
import autopopulate from 'mongoose-autopopulate'

const userSchema = new Schema({
  rfid: {
    type: String,
    unique: true,
    sparse: true
  },
  studentId: {
    type: String,
    required: true,
    unique: true
  },
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  isAdmin: {
    type: Boolean,
    required: true,
    default: false
  },
  requests: [{
    type: Schema.Types.ObjectId,
    ref: 'Request',
    autopopulate: true,
    required: true
  }],
  notification: {
    type: String
  }
})

userSchema.plugin(autopopulate)

module.exports = mongoose.model('User', userSchema)
