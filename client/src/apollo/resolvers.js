import { gql } from 'apollo-boost'
import defaults from './defaults.js'

const formFieldsMap = {
  signInForm: `{
    firstName
    lastName
    studentId
    password
  }`,
  itemForm: `{
    id
    name
    count
    defectiveCount
    barcode
  }`
}

export default {
  Mutation: {
    clearRequests: (_, variables, { cache }) => {
      cache.writeData({
        data: {
          requestForm: {
            __typename: 'Form',
            items: []
          }
        }
      })
      return null
    },
    removeItem: (_, { id }, { cache }) => {
      const query = gql`{ requestForm { items { id, name, availableCount, requestCount } } }`
      const data = cache.readQuery({ query })
      const foundItem = data.requestForm.items.findIndex((requestItem) => requestItem.id === id)

      data.requestForm.items.splice(foundItem, 1)
      cache.writeData({ data })
      return null
    },
    decreaseItem: (_, { id }, { cache }) => {
      const query = gql`{ requestForm { items { id, name, availableCount, requestCount } } }`
      const data = cache.readQuery({ query })
      const foundItem = data.requestForm.items.find((requestItem) => requestItem.id === id)

      if (foundItem.requestCount > 1) {
        foundItem.requestCount--
        cache.writeData({ data })
        return null
      }
      return null
    },
    requestItem: (_, { item }, { cache }) => {
      const { id, name, availableCount } = item

      if (availableCount > 0) {
        const query = gql`{ requestForm { items { id, name, availableCount, requestCount } } }`
        const data = cache.readQuery({ query })
        const foundItem = data.requestForm.items.find((requestItem) => requestItem.id === id)

        if (foundItem) {
          if (foundItem.availableCount > foundItem.requestCount) {
            foundItem.requestCount++
          }
        } else {
          data.requestForm.items.push({
            __typename: 'Item', id, name, availableCount, requestCount: 1
          })
        }
        cache.writeData({ data })
        return null
      }
      return null
    },
    updateForm: (_, { id, field }, { cache }) => {
      const query = gql`{ ${id} { visible fields ${formFieldsMap[id]} } }`
      const data = cache.readQuery({ query })
      data[id] = {
        ...data[id],
        fields: {
          ...data[id].fields,
          ...field
        }
      }
      cache.writeData({ data })
      return null
    },
    resetForm: (_, { id }, { cache }) => {
      const data = {
        [id]: {
          ...defaults[id],
          visible: true
        }
      }
      cache.writeData({ data })
      return null
    },
    toggleForm: (_, { id }, { cache }) => {
      const query = gql`{ ${id} { visible } }`
      const data = cache.readQuery({ query })
      data[id].visible = !data[id].visible
      cache.writeData({ data })
      return null
    }
  }
}