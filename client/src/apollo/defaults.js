const defaults = {
  itemForm: {
    __typename: 'Form',
    visible: false,
    fields: {
      __typename: 'Fields',
      id: '',
      name: '',
      barcode: '',
      count: 1,
      defectiveCount: 0
    }
  },
  signInForm: {
    __typename: 'Form',
    visible: false,
    fields: {
      __typename: 'Fields',
      firstName: '',
      lastName: '',
      studentId: '',
      password: ''
    }
  },
  requestForm: {
    __typename: 'Form',
    visible: false,
    items: []
  }
}

export default defaults
