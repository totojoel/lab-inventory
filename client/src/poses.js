import posed from 'react-pose'

export const BumpPose = posed.div({
  enter: {
    y: 0, opacity: 1,
    transition: {
      y: { type: 'spring', stiffness: 400, damping: 15 },
    }
  },
  exit: {
    y: '-8px', opacity: 0, transition: {
      y: { type: 'spring', stiffness: 400, damping: 15 },
    }
  }
})

export const MaximizePose = posed.div({
  opened: {
    height: '280px',
    transition: {
      height: { type: 'spring', stiffness: 100, damping: 15 },
    }
  },
  closed: {
    height: '0',
    transition: {
      y: { type: 'spring', stiffness: 100, damping: 15 },
    }
  }
})
