import { gql } from 'apollo-boost'

export const CLOSE_REQUEST = gql`
  mutation {
    closeRequest @client
  }
`

export const ADVANCE_REQUEST = gql`
  mutation($requestId: String!, $status: String!) {
    advanceRequest(requestId: $requestId, status: $status) {
      status
    }
  }
`

export const UPDATE_FORM = gql`
  mutation($form: String!, $field: Object!) {
    updateForm(id: $form, field: $field) @client
  }
`

export const TOGGLE_FORM = gql`
  mutation($form: String!) {
    toggleForm(id: $form) @client
  }
`

export const RESET_FORM = gql`
  mutation($form: String!) {
    resetForm(id: $form) @client
  }
`

export const TOGGLE_REQUEST_FORM = gql`
  mutation {
    toggleRequestForm @client
  }
`

export const SAVE_REQUEST_ITEMS = gql`
  mutation($items: String) {
    saveRequestItems(items: $items) @client
  }
`

export const REQUEST_ITEM = gql`
  mutation($item: Object) {
    requestItem(item: $item) @client
  }
`

export const DECREASE_ITEM = gql`
  mutation($item: Object) {
    decreaseItem(id: $item) @client
  }
`

export const REMOVE_ITEM = gql`
  mutation($item: Object) {
    removeItem(id: $item) @client
  }
`

export const CLEAR_REQUESTS = gql`
  mutation {
    clearRequests @client
  }
`
