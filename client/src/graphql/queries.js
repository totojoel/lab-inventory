import { gql } from 'apollo-boost'

export const GET_ACTIVE_USER = gql`
  query {
    user {
      studentId
      firstName
      isAdmin
    }
  }
`

export const GET_SCANNED_ID = gql`
  query {
    scannedId
  }
`
