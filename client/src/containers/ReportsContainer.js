import React from 'react'

import Alert from '../components/Alert'
import Reports from '../components/Reports'

const ReportsContainer = ({ user }) => {
  const isAdmin = user ? user.isAdmin : false

  if (!isAdmin) {
    return <Alert alert={{ type: 'error', message: 'You are not authorized.' }} />
  }
  if (user) {
    return <Reports />  
  }
}

export default ReportsContainer
