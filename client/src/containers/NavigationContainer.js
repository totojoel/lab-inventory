import React from 'react'
import { graphql, compose } from 'react-apollo'
import history from '../history'
import { withRouter } from 'react-router-dom'

import { TOGGLE_FORM } from '../graphql/mutations'

import Navigation from '../components/Navigation'

const NavigationContainer = ({ location, user, toggleForm }) => {
  const isAdmin = user ? user.isAdmin : false

  let items = [
    { link: '/inventory', text: 'Inventory', icon: 'home', enabled: true },
    { link: '/requests', text: 'Requests', icon: 'description', enabled: isAdmin },
    { link: '/reports/', text: 'Reports', icon: 'show_chart', enabled: isAdmin }
  ]

  items = items.map(
    item => ({
      ...item,
      active: location.pathname.startsWith(item.link),
      onClick: () => { history.push(item.link) }
    })
  )

  return (
    <Navigation
      user={user}
      toggleForm={toggleForm}
      items={items}
    />
  )
}

export default compose(
  graphql(TOGGLE_FORM, { name: 'toggleForm' })
)(withRouter(NavigationContainer))
