import React, { Component } from 'react'
import styled from 'styled-components'
import { gql } from 'apollo-boost'
import { graphql, compose } from 'react-apollo'

import { TOGGLE_FORM, CLEAR_REQUESTS } from '../graphql/mutations'

import RequestFormList from '../components/RequestFormList'
import Drawer from '../components/Drawer'
import Buttons from '../components/Buttons'
import Loading from '../components/Loading'
import Alert from '../components/Alert'

const ADD_REQUEST = gql`
  mutation($items: [ItemInput]) {
    addRequest(items: $items) {
      items {
        id
      }
    }
  }
`

const Wrapper = styled.div({
  display: 'flex',
  height: '100%',
  position: 'relative',
  paddingLeft: '5px',
  paddingRight: '5px',
  flexDirection: 'column'
})

const ButtonsWrap = styled.div({
  // background: 'linear-gradient(transparent, #111, #111)'
})

class RequestFormContainer extends Component {
  state = {
    alert: null,
    loading: false
  }

  handleSuccess = (result) => {
    this.props.clear()
    this.setState({
      loading: false,
      alert: { type: 'success', message: 'You request is submitted. Please wait while your items are being prepared.' }
    })
  }

  handleError = (error) => {
    this.setState({
      alert: { type: 'error', message: error.message },
      loading: false
    })
  }

  submit = () => {
    this.setState({ loading: true })
    const items = this.props.items.map(item => ({ id: item.id, requestCount: item.requestCount }))
    if (!this.props.user) {
      this.props.toggle({ variables: { form: 'signInForm' } })
    }
    if (items.length) {
      this.props.addRequest({ variables: { items } })
        .then(this.handleSuccess)
        .catch(this.handleError)
    } else {
      this.setState({
        loading: false,
        alert: { type: 'error', message: 'You have not added any item.' }
      })
    }
  }

  reset = () => {
    this.props.clear()
    this.setState({ alert: null, loading: false, success: false })
  }

  toggle = () => {
    this.props.toggle({ variables: { form: 'requestForm' } })
  }

  render() {
    const { visible, items } = this.props
    const { alert, loading } = this.state
    const buttons = [
      { text: 'Clear', primary: false, onClick: this.reset },
      { text: 'Submit', primary: true, onClick: this.submit }
    ]

    if (items) {
      return (
        <Drawer
          open={visible}
          toggle={this.toggle}
          icon='description'
          title={`Request ${items.length}`}
          renderContent={() => (
            <Wrapper>
              {alert && <Alert alert={alert} margin='0 0 10px 0' />}
              <RequestFormList items={items} />
              <ButtonsWrap><Buttons buttons={buttons} /></ButtonsWrap>
              <Loading visible={loading} />
            </Wrapper>
          )}
        />
      )
    }
    return <Loading visible={true} />
  }
}

export default compose(
  graphql(gql`{
    requestForm @client {
      visible
      items {
        id
        name
        availableCount
        requestCount
      }
    }
  }`, {
      props: ({ data: { requestForm } }) => ({
        ...requestForm
      })
    }
  ),
  graphql(TOGGLE_FORM, { name: 'toggle' }),
  graphql(CLEAR_REQUESTS, { name: 'clear' }),
  graphql(ADD_REQUEST, { name: 'addRequest' })
)(RequestFormContainer)

