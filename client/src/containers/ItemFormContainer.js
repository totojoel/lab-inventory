import React, { Component } from 'react'
import styled from 'styled-components'
import { gql } from 'apollo-boost'
import { graphql, compose } from 'react-apollo'
import BarcodeReader from 'react-barcode-reader'

import { TOGGLE_FORM, UPDATE_FORM, RESET_FORM } from '../graphql/mutations'

import { validateItem } from '../validators'

import Modal from '../components/Modal'
import Alert from '../components/Alert'
import Buttons from '../components/Buttons'
import Form from '../components/Form'
import Loading from '../components/Loading'
import Icon from '../components/Icon'

const Message = styled.div({
  marginTop: '-10px',
  marginBottom: '20px',
  fontSize: '12px',
})

const ScannedItemWrap = styled.div(({ item }) => ({
  overflow: 'hidden',
  whiteSpace: item ? 'nowrap' : 'normal',
  background: '#191919',
  marginRight: '10px',
  textTransform: item ? 'uppercase' : 'unset',
  fontFamily: item ? 'Code39' : 'Roboto',
  fontSize: item ? '32px' : '12px',
  display: 'flex',
  justifyContent: 'center',
  paddingLeft: '10px',
  paddingRight: '10px',
  textAlign: 'center',
  alignItems: 'center'
}))

const Wrapper = styled.div({
  display: 'grid',
  position: 'relative',
  gridTemplateColumns: '40% 60%',
})

const SAVE_ITEM = gql`
  mutation($id: String, $name: String!, $count: Int!, $barcode: String!, $defectiveCount: Int) {
    saveItem(id: $id, name: $name, count: $count, barcode: $barcode, defectiveCount: $defectiveCount) {
      name
    }
  }
`

const ScannedItem = ({ item }) => (
  <ScannedItemWrap item={item}>
    {item ? item :
      <div>
        <Icon name='block' size='18px' margin='0 0 4px 0' />
        <div>Please scan</div>
        <div>or type barcode.</div>
      </div>}
  </ScannedItemWrap>
)

class ItemFormContainer extends Component {
  state = {
    alert: null,
    loading: false
  }

  close = () => {
    this.reset()
      .then(() => {
        this.props.toggle({ variables: { form: 'itemForm' } })
      })
  }

  reset = () => {
    this.setState({
      loading: false,
      alert: null
    })
    return this.props.reset({ variables: { form: 'itemForm' } })
  }

  handleError = (error) => {
    this.setState({
      alert: { type: 'error', message: error.message },
      loading: false
    })
  }

  handleSuccess = (name) => {
    this.props.reset({ variables: { form: 'itemForm' } })
    this.setState({
      alert: { type: 'success', message: name + ' is saved!' },
      loading: false
    })
  }

  submit = () => {
    this.setState({ loading: true })
    this.props.saveItem({ variables: this.props.form.fields })
      .then(() => {
        this.handleSuccess(this.props.form.fields.name)
      })
      .catch(this.handleError)
  }

  update = (field) => {
    this.props.update({ variables: { form: 'itemForm', field } })
  }

  render() {
    const { alert, loading } = this.state
    const { form: { visible, fields } } = this.props
    const items = [
      {
        key: 1,
        fields: [
          {
            id: 'name',
            label: 'Item',
            type: 'text',
            required: true,
            validate: validateItem
          }
        ]
      },
      {
        key: 2,
        fields: [
          {
            id: 'barcode',
            label: 'Barcode',
            type: 'text',
            placeholder: 'Type or scan',
            required: true,
            validate: validateItem
          }
        ]
      },
      {
        key: 3,
        fields: [
          {
            id: 'count',
            label: 'Units stored',
            type: 'count',
            required: true,
          }
        ]
      },
      {
        key: 4,
        fields: [
          {
            id: 'defectiveCount',
            label: 'Units defective',
            type: 'count',
            required: true,
          }
        ]
      }
    ]

    const buttons = [
      { text: 'Cancel', primary: false, onClick: this.close },
      { text: 'Save', primary: true, onClick: this.submit }
    ]

    return (
      <Modal
        visible={visible}
        close={this.close}
        title='Update inventory'
        icon='add'
        renderContent={() => (
          <div>
            <Message>Please fill out this form to add or update items.</Message>
            {alert && <Alert alert={alert} margin='0 0 10px 0' />}
            <Wrapper>
              <BarcodeReader
                onError={(error) => this.setState({ error: error.message })}
                onScan={(data) => this.update({ barcode: data })}
              />
              <ScannedItem item={fields.barcode} />
              <Form
                items={items.sort((a, b) => a.key > b.key)}
                values={fields}
                update={this.update}
                submit={this.submit}
                close={this.close}
              />
              {loading && <Loading visible={loading} message='Working' />}
            </Wrapper>
            <Buttons buttons={buttons} />
          </div>
        )}
      />
    )
  }
}

export default compose(
  graphql(gql`{
    itemForm @client {
      visible
      fields {
        id
        name
        count
        defectiveCount
        barcode
      }
    }
  }`, {
      props: ({ data: { itemForm } }) => ({
        form: { ...itemForm }
      })
    }
  ),
  graphql(RESET_FORM, { name: 'reset' }),
  graphql(UPDATE_FORM, { name: 'update' }),
  graphql(TOGGLE_FORM, { name: 'toggle' }),
  graphql(SAVE_ITEM, { name: 'saveItem' })
)(ItemFormContainer)
