import React from 'react'
import history from '../history'
import { withRouter } from 'react-router-dom'
import { gql } from 'apollo-boost'
import { Query } from 'react-apollo'

import Report from '../components/Report'

const GET_REPORT = gql`
  query ($from: Float, $to: Float) {
    items (
      from: $from, to: $to
    )  {
      name
      count
      borrowedCount
      defectiveCount
      availableCount
    }
  }
`

const ItemsReportContainer = ({ location, variables }) => (
  <Query query={GET_REPORT} variables={variables} pollInterval={1000}>
    {({ data }) => {
      let rows = data && data.items ? data.items.map((item, index) => {
        const { name, count, borrowedCount, defectiveCount, availableCount } = item
        return [name, count, borrowedCount, defectiveCount, availableCount]
      }) : []

      const params = new URLSearchParams(history.location.search)
      const sort = params.get('sort')

      if (sort) {
        rows.sort((a, b) => {
          if (sort === '0') {
            return a[sort] > b[sort] ? 1 : -1
          }
          return b[sort] - a[sort]
        })
      }

      return <Report
        title='Items' icon='storage'
        onClick={() => history.push(`/reports/items`)}
        active={location.pathname.match('items')}
        header={['Item', 'Units Stored', 'Units Borrowed', 'Units Defective', 'Units Available']}
        rows={rows}
      />
    }}
  </Query>
)

export default withRouter(ItemsReportContainer)
