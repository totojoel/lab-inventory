import React, { Component } from 'react'
import styled from 'styled-components'
import { gql } from 'apollo-boost'
import { graphql, compose } from 'react-apollo'
import history from '../history'
import { withRouter } from 'react-router-dom'

import Modal from '../components/Modal'
import Buttons from '../components/Buttons'
import Alert from '../components/Alert'
import Loading from '../components/Loading'

const DELETE_ITEM = gql`
  mutation($id: String!) {
    deleteItem(id: $id) {
      name
    }
  }
`

const Wrapper = styled.div({
  fontSize: '14px'
})

class ConfirmContainer extends Component {
  state = {
    visible: false,
    loading: false,
    alert: null
  }

  componentDidMount() {
    setTimeout(() => this.setState({ visible: true }), 10)  // delay for entrance
  }

  close = () => {
    const { match } = this.props
    setTimeout(() => history.push(match.params[0]), 300) // delay for exit
    this.setState({ visible: false, loading: false, alert: null })
  }

  handleError = (error) => {
    this.setState({
      alert: { type: 'error', message: error.message },
      loading: false
    })
  }

  confirm = () => {
    this.setState({ loading: true })
    const params = new URLSearchParams(history.location.search)
    this.props.deleteItem({ variables: { id: params.get('id') } })
      .then(this.close)
      .catch(this.handleError)
  }

  render() {
    const buttons = [
      { text: 'Cancel', primary: false, onClick: this.close },
      { text: 'Confirm', primary: true, onClick: this.confirm }
    ]
    const { visible, alert, loading } = this.state
    const params = new URLSearchParams(history.location.search)

    return (
      <Modal
        visible={visible}
        title={`Delete ${params.get('name')}`}
        icon='warning'
        close={this.close}
        renderContent={() => (
          <div>
            {alert && <Alert alert={alert} margin='0 0 10px 0' />}
            <Loading visible={loading} />
            <Wrapper>Are you sure you want to delete {params.get('name')}?</Wrapper>
            <Buttons buttons={buttons} />
          </div>
        )}
      />
    )
  }
}

export default compose(
  graphql(DELETE_ITEM, { name: 'deleteItem' })
)(withRouter(ConfirmContainer))
