import React from 'react'
import styled from 'styled-components'
import { Query } from 'react-apollo'
import { gql } from 'apollo-boost'

import Loading from '../components/Loading'
import { Alert } from 'antd'
import RequestList from '../components/RequestList'

const GET_REQUESTS = gql`
  query($status: String) {
    requests (
      status: $status
    ) {
      id
      status
      student {
        studentId
        firstName
        lastName
      }
      dateRequested
      dateApproved
      dateReturned
    }
  }
`

const Wrapper = styled.div({
  display: 'flex'
})

// const RequestsContainer = () => {
//   return <Wrapper>Requests here</Wrapper>
// }

const RequestsContainer = ({ status }) => {
  return (
    <Wrapper>
      <Query query={GET_REQUESTS} variables={status ? { status } : null} pollInterval={800} >
        {({ loading, error, data }) => {
          if (loading) return <Loading message='Loading requests' />
          if (error) return <Alert message={error.message} type='error' showIcon banner closable />
          console.log(data.requests)
          return <RequestList status={status} requests={data.requests} />
        }}
      </Query>
    </Wrapper>
  )
}


export default RequestsContainer
