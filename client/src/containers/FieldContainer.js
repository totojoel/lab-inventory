import React, { Component } from 'react'

import Field from '../components/Field'

class FieldContainer extends Component {
  onChange = (value) => {
    const { id, onChange, type } = this.props
    if (type === 'count') {
      const numVal = +value
      if (numVal >= 0 && numVal < 999999999999999) {
        onChange({ [id]: numVal })
      }
    } else {
      onChange({ [id]: value })
    }
  }

  render() {
    const { label, value, error, type, placeholder } = this.props

    return <Field
      label={label}
      placeholder={placeholder}
      type={type}
      value={value}
      error={error}
      onChange={this.onChange}
    />
  }
}

export default FieldContainer

