import React from 'react'

import RequestsBoard from '../components/RequestsBoard'
import Alert from '../components/Alert'

const RequestsBoardContainer = ({ user }) => {
  const isAdmin = user ? user.isAdmin : false
  const statuses = ['pending', 'ready', 'received', 'returned']

  if (!isAdmin) {
    return <Alert alert={{ type: 'error', message: 'You are not authorized.' }} />
  }
  if (user) {
    return <RequestsBoard statuses={statuses} />
  }
}

export default RequestsBoardContainer
