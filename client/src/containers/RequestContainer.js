import React from 'react'
import { gql } from 'apollo-boost'
import { graphql, compose, Query } from 'react-apollo'
import history from '../history'

import { ADVANCE_REQUEST } from '../graphql/mutations'

import Request from '../components/Request'

const GET_REQUEST = gql`
  query($id: String){
    request (
      id: $id
    ) {
      id
      status
      dateRequested
      dateApproved
      dateReceived
      dateReturned
      student {
        firstName
        lastName
      }
      items {
        item {
          id
          name
          barcode
        }
        count
      }
    }
  }
`

const RequestContainer = ({ match, advance }) => {
  const id = match.params.id
  return (
    <Query query={GET_REQUEST} variables={{ id }} pollInterval={800} >
      {({ data, error }) => {
        const props = {
          close: () => history.push('/requests'),
          advance: advance,
          error,
          request: data ? data.request : null
        }
        return <Request {...props} />
      }}
    </Query>
  )
}

export default compose(
  graphql(ADVANCE_REQUEST, { name: 'advance' })
)(RequestContainer)
