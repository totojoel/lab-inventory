import React from 'react'
import { Query } from 'react-apollo'
import { gql } from 'apollo-boost'
import history from '../history'

import Inventory from '../components/Inventory'

const GET_ITEMS = gql`
  query($name: String) {
    items (
      name: $name
    ) {
      id
      name
      count
      barcode
      dateAdded
      dateUpdated
      requestedCount
      availableCount
      defectiveCount
    }
  }
`

const InventoryContainer = ({ user }) => {
  const params = new URLSearchParams(history.location.search)
  const name = params.get('item')

  return (
    <Query query={GET_ITEMS} variables={{ name: name ? name : '' }} pollInterval={800}>
      {({ loading, error, data }) => {
        const props = {
          loading, user,
          items: data ? data.items : [],
          alert: error ? {
            type: 'error',
            message: 'Failed to fetch items.'
          } : null
        }
        return <Inventory {...props} />
      }}
    </Query>
  )
}

export default InventoryContainer
