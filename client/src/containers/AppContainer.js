import React from 'react'
import { graphql, compose } from 'react-apollo'

import { GET_ACTIVE_USER } from '../graphql/queries'

import App from '../components/App'

const AppContainer = ({ user }) => (
  <App user={user} />
)

export default compose(
  graphql(GET_ACTIVE_USER, {
    props: ({ data: { user } }) => ({
      user
    }),
    options: () => ({
      pollInterval: 800
    })
  })
)(AppContainer)
