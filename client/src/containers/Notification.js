import React from 'react'
import { Query } from 'react-apollo'
import { gql } from 'apollo-boost'

const NotificationList = () => {
  const params = new URLSearchParams(history.location.search)
  const name = params.get('item')

  return (
    <Query query={GET_ITEMS} variables={{ name: name ? name : '' }} pollInterval={800}>
      {({ loading, error, data }) => {
        const props = {
          loading, user,
          items: data ? data.items : [],
          alert: error ? {
            type: 'error',
            message: 'Failed to fetch items.'
          } : null
        }
        return <Inventory {...props} />
      }}
    </Query>
  )
}

export default InventoryContainer
