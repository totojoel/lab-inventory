import React from 'react'
import history from '../history'
import { withRouter } from 'react-router-dom'
import { gql } from 'apollo-boost'
import { Query } from 'react-apollo'
import moment from 'moment'

import Report from '../components/Report'

const GET_REPORT = gql`
  query ($from: Float, $to: Float) {
    requests (
      from: $from, to: $to
    ) {
      student {
        lastName
        firstName
        studentId
      }
      items {
        item {
          name
        }
      }
      dateRequested
      status
    }
  }
`

const RequestsReportContainer = ({ location, variables }) => (
  <Query query={GET_REPORT} variables={variables} pollInterval={1000}>
    {({ data }) => {
      let rows = data && data.requests ? data.requests.map((request, index) => {
        const { student, items, status, dateRequested } = request
        let studentId = student ? student.studentId : 'Deleted'
        let name = student ? `${student.firstName} ${student.lastName}` : 'Deleted'

        return [studentId, name, items.map(item => item.item ? item.item.name : 'Deleted').join(', '), status, dateRequested && moment(dateRequested).format('LL')]
      }) : []

      const params = new URLSearchParams(history.location.search)
      const sort = params.get('sort')

      if (sort) {
        rows.sort((a, b) => {
          let first = a[sort]
          let second = b[sort]
          if (sort === '4') {
            first = new Date(second).getTime()
            second = new Date(first).getTime()
          }

          return first > second ? 1 : first < second ? -1 : 0
        })
      }

      return <Report
        title='Requests' icon='description'
        onClick={() => history.push(`/reports/requests`)}
        active={location.pathname.match('requests')}
        header={['ID', 'Student', 'Items', 'Status', 'Date Requested']}
        rows={rows}
      />
    }}
  </Query>
)

export default withRouter(RequestsReportContainer)
