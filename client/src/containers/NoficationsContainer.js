import React, { Component } from 'react'
import styled from 'styled-components'
import { Query } from 'react-apollo'
import { gql } from 'apollo-boost'

import Alert from '../components/Alert'

const GET_USER_NOTIFICATION = gql`
  query {
    user {
      notification
    }
  }
`

const AlertHolder = styled.div({
  position: 'fixed',
  right: '100px',
  top: '76px',
  zIndex: 5000
})

class Notification extends Component {
  render() {
    const { message } = this.props
    const alert = { type: 'success', message }

    return (
      <AlertHolder style={{ display: message ? 'unset' : 'none' }} ><Alert alert={alert} /></AlertHolder>
    )
  }
}

const NotificationContainer = () => {
  return (
    <Query query={GET_USER_NOTIFICATION} pollInterval={100}>
      {({ loading, error, data }) => {
        return <Notification message={data.user ? data.user.notification || '' :  ''} />
      }}
    </Query>
  )
}

export default NotificationContainer
