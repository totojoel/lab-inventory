import React from 'react'
import history from '../history'

import SearchBox from '../components/SearchBox'

const SearchBoxContainer = ({ value, onChange, onKeyPress }) => {
  const params = new URLSearchParams(history.location.search)
  return (
    <SearchBox
      value={value ? value : params.get('item')}
      onChange={onChange}
      onKeyPress={onKeyPress}     
    />
  )
}

export default SearchBoxContainer
