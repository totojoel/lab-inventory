import React from 'react'
import { Query } from 'react-apollo'
import { gql } from 'apollo-boost'

import RequestsList from '../components/RequestsList'

const GET_REQUESTS = gql`
  query($status: String) {
    requests (
      status: $status
    ) {
      id
      status
      student {
        firstName
        lastName
      }
      items {
        count
      }
      dateRequested
    }
  }
`

const RequestsListContainer = ({ status, history }) => {
  return (
    <Query
      query={GET_REQUESTS}
      variables={{
        status
      }}
      pollInterval={800}>
      {({ data, loading }) => (
        <RequestsList
          name={status}
          history={history}
          loading={loading}
          requests={data ? data.requests : []}
        />
      )}
    </Query>
  )
}

export default RequestsListContainer
