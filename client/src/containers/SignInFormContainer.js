import React, { Component } from 'react'
import styled from 'styled-components'
import { gql } from 'apollo-boost'
import { graphql, compose } from 'react-apollo'

import { TOGGLE_FORM, UPDATE_FORM, RESET_FORM } from '../graphql/mutations'
import { GET_SCANNED_ID } from '../graphql/queries'

import { signIn as saveCurrentUser } from '../actions/user'
import { validateName, validatePassword, validateStudentNumber } from '../validators'

import Modal from '../components/Modal'
import Alert from '../components/Alert'
import Form from '../components/Form'
import Buttons from '../components/Buttons'
import Loading from '../components/Loading'

const Message = styled.div({
  marginTop: '-10px',
  marginBottom: '20px',
  fontSize: '12px',
})

const ClickableText = styled.u({
  '&:hover': {
    cursor: 'pointer'
  }
})

const ADD_USER = gql`
  mutation(
    $studentId: String!, 
    $firstName: String!,
    $lastName: String!,
    $password: String!,
    $rfid: String
  ) {
    addUser(
      studentId: $studentId,
      firstName: $firstName,
      lastName: $lastName,
      password: $password,
      rfid: $rfid 
    ) {
      firstName
    }
  }
`

const SIGN_IN = gql`
  mutation($studentId: String!, $password: String!) { 
    token: signUser(studentId: $studentId, password: $password)
  }
`

const SIGN_IN_BY_ID = gql`
  mutation($rfid: String!) { 
    token: signUserById(rfid: $rfid)
  }
`

const ScannedIdWrap = styled.div(({ id }) => ({
  border: id ? 'solid 1px #00ccff' : 'none',
  background: '#191919',
  color: id ? '#00ccff' : '#fafafa',
  marginTop: '10px',
  fontSize: '14px',
  display: 'flex',
  justifyContent: 'center',
  paddingTop: '2px',
  height: '30px',
  alignItems: 'center',
  transition: '500ms'
}))

const Wrapper = styled.div({
  position: 'relative'
})

const ScannedId = ({ id, isSigningIn }) => (
  <ScannedIdWrap id={id}>
    {id ? 'Your ID is scanned!' : isSigningIn ? 'Or you may also just scan your ID!' :
      "Please scan your ID."}
  </ScannedIdWrap>
)

class SignInFormContainer extends Component {
  state = {
    alert: null,
    loading: false,
    isSigningIn: true,
    warned: false
  }

  componentDidUpdate(prevProps) {
    if (prevProps.scannedId !== this.props.scannedId) {
      const { loading, isSigningIn } = this.state
      const { form: { visible }, scannedId } = this.props
      if (!loading && isSigningIn && visible && scannedId) {
        this.submit()
      }
    }
  }

  toggleIsSigningIn = () => {
    this.reset()
    this.setState({ isSigningIn: !this.state.isSigningIn, alert: null, loading: false, warned: false })
  }

  handleError = (error) => {
    this.setState({
      alert: { type: 'error', message: error.message },
      loading: false,
      warned: false
    })
  }

  handleSuccess = (result) => {
    this.close()
    saveCurrentUser(result.data)
  }

  submit = () => {
    const { form: { fields }, signUp, signIn, signInById, scannedId } = this.props
    const { isSigningIn, warned } = this.state

    this.setState({ loading: true })
    if (isSigningIn) {
      if (scannedId) {
        signInById({ variables: { rfid: scannedId } })
          .then(this.handleSuccess)
          .catch(this.handleError)
      } else {
        signIn({ variables: { ...fields } })
          .then(this.handleSuccess)
          .catch(this.handleError)
      }
    } else {
      if (scannedId || (!scannedId && warned)) {
        signUp({ variables: { ...fields, rfid: scannedId ? scannedId : undefined } })
          .then(({ data }) => {
            this.update({ password: '' })
            this.setState({
              alert: { type: 'success', message: `Hello, ${data.addUser.firstName}! Please sign in.` },
              loading: false,
              warned: false,
              isSigningIn: true,
            })
          })
          .catch(this.handleError)
      } else {
        this.setState({
          alert: { type: 'warning', message: "Are you sure you won't scan your ID?" },
          loading: false,
          warned: true
        })
      }
    }
  }

  close = () => {
    this.reset()
      .then(() => {
        this.props.toggle({ variables: { form: 'signInForm' } })
      })
  }

  reset = () => {
    this.setState({
      loading: false,
      alert: null,
      isSigningIn: true,
      warned: false
    })
    return this.props.reset({ variables: { form: 'signInForm' } })
  }

  update = (field) => {
    this.props.update({ variables: { form: 'signInForm', field } })
  }

  render() {
    const { isSigningIn, alert, loading } = this.state
    const { form: { visible, fields }, scannedId } = this.props

    const items = [
      {
        key: 2,
        fields: [
          {
            id: 'studentId',
            label: 'Student Number',
            placeholder: 'XX-XXXX-XX',
            required: true,
            type: 'text',
            validate: validateStudentNumber
          }
        ]
      },
      {
        key: 3,
        fields: [
          {
            id: 'password',
            label: 'Password',
            type: 'password',
            required: true,
            validate: validatePassword
          }
        ]
      }
    ]

    if (!isSigningIn) {
      items.push(
        {
          key: 1,
          fields: [
            {
              id: 'firstName',
              label: 'First Name',
              required: true,
              type: 'text',
              validate: validateName,
            },
            {
              id: 'lastName',
              label: 'Last Name',
              required: true,
              type: 'text',
              validate: validateName,
            }
          ]
        }
      )
    }

    const buttons = [
      { text: 'Cancel', primary: false, onClick: this.close },
      {
        text: isSigningIn ? 'Sign in' : 'Sign up',
        primary: true,
        onClick: this.submit
      }
    ]

    return (
      <Modal
        visible={visible}
        close={this.close}
        title={isSigningIn ? 'Sign in' : 'Sign up'}
        icon='account_box'
        renderContent={() => (
          <div>
            {isSigningIn ?
              <Message>
                <div>Please sign in.</div>
                <div>No account? No problem! <ClickableText onClick={this.toggleIsSigningIn}>
                  Sign up</ClickableText>.
                        </div>
              </Message> :
              <Message>
                <div>Please complete this form to sign up.</div>
                <div>Already have an account? Nice! <ClickableText onClick={this.toggleIsSigningIn}>
                  Sign in</ClickableText>.
                        </div>
              </Message>
            }
            {alert && <Alert alert={alert} margin='0 0 10px 0' />}
            <Wrapper>
              <Form
                items={items.sort((a, b) => a.key - b.key)}
                values={fields}
                update={this.update}
                submit={this.submit}
                close={this.close}
              />
              <ScannedId id={scannedId} isSigningIn={isSigningIn} />
              {loading && <Loading visible={loading} message='Working' />}
            </Wrapper>
            <Buttons buttons={buttons} />
          </div>
        )}
      />
    )
  }
}

export default compose(
  graphql(gql`{
    signInForm @client {
      visible
      fields {
        firstName
        lastName
        studentId
        password
      }
    }
  }`, {
      props: ({ data: { signInForm } }) => ({
        form: { ...signInForm }
      })
    }
  ),
  graphql(GET_SCANNED_ID, {
    props: ({ data: { scannedId } }) => ({
      scannedId
    }),
    options: () => ({
      pollInterval: 800
    })
  }),
  graphql(RESET_FORM, { name: 'reset' }),
  graphql(TOGGLE_FORM, { name: 'toggle' }),
  graphql(UPDATE_FORM, { name: 'update' }),
  graphql(SIGN_IN_BY_ID, { name: 'signInById' }),
  graphql(SIGN_IN, { name: 'signIn' }),
  graphql(ADD_USER, { name: 'signUp' })
)(SignInFormContainer)
