import React, { Component } from 'react'
import styled from 'styled-components'
import { PoseGroup } from 'react-pose'

import { BumpPose } from '../poses'

import Actions from './Actions'

const Card = styled.div({
  background: '#fafafa',
  color: '#222222',
  opacity: 0.9,
  display: 'grid',
  float: 'left',
  padding: '10px',
  marginRight: '10px',
  marginBottom: '10px',
  '&:hover': {
    cursor: 'pointer',
    opacity: 1
  }
})

const Title = styled.span({
  fontSize: '14px'
})

const Info = styled.div({
  padding: '10px',
  width: '160px',
  paddingTop: 0,
  display: 'flex',
  flexDirection: 'column',
  fontSize: '12px'
})

const InfoWrapper = styled.div({
  display: 'grid'
})

class Item extends Component {
  state = {
    visible: false
  }

  componentDidMount() {
    setTimeout(() => this.setState({ visible: true }), 10)  // delay for entrance
  }

  render() {
    const { item, request, update, discard, isAdmin } = this.props
    const actions = [
      { text: 'Request', icon: 'add_circle', onClick: () => isAdmin && request(item) }
    ]
    const adminActions = [
      { text: 'Delete', icon: 'delete', onClick: () => discard(item) },
      { text: 'Update', icon: 'edit', onClick: () => update(item) }
    ]

    return (
      <PoseGroup>{
        this.state.visible && <BumpPose key='1'><Card onClick={isAdmin ? null : () => request(item)}>
          <Actions actions={isAdmin ? adminActions.concat(actions) : actions} />
          <InfoWrapper>
            <Info>
              <Title>{item.name}</Title>
              <span>{item.availableCount} available units</span>
              <span>{item.requestedCount} requested units</span>
            </Info>
          </InfoWrapper>
        </Card></BumpPose>
      }</PoseGroup>
    )
  }
}

export default Item
