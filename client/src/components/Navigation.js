import React from 'react'
import styled from 'styled-components'
import history from '../history'

import { signOut } from '../actions/user'

import NavigationItem from './NavigationItem'

const Wrapper = styled.div({
  position: 'absolute',
  height: '100px',
  width: '100%',
  paddingRight: '100px',
  paddingLeft: '100px',
  display: 'flex',
  alignItems: 'center',
  zIndex: '1000',
  whiteSpace: 'nowrap'
})

const Title = styled.span({
  fontFamily: '"VCR", monospace',
  textTransform: 'uppercase',
  fontSize: '28px',
  paddingBottom: '4px',
  transition: '300ms',
  '&:hover': {
    cursor: 'pointer',
    color: '#00ccff'
  }
})

const Navigation = ({ user, toggleForm, items }) => {
  return (
    <Wrapper>
      <Title onClick={() => history.push('/')}>Electronics Laboratory</Title>
      {items.map(item => <NavigationItem key={item.link} {...item} />)}
      {user && <NavigationItem
        icon='account_circle'
        fixed
        text={user.firstName}
        enabled
      />}
      <NavigationItem
        text={user ? 'Sign out' : 'Sign in'}
        onClick={user ? () => signOut() : () => toggleForm({ variables: { form: 'signInForm' } })}
        enabled
        fixed
      />
    </Wrapper>
  )
}

export default Navigation