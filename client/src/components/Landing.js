import React, { Component } from 'react'
import styled from 'styled-components'
import history from '../history'
import banner from '../assets/banner-inverted-flipped.png'

import SearchBoxContainer from '../containers/SearchBoxContainer'

const Wrapper = styled.div({
  display: 'flex',
  height: '100%',
  width: '100%',
  alignItems: 'center',
  justifyContent: 'center'
})

const Banner = styled.img({
  position: 'absolute',
  height: '100%',
  top: '-50px',
  right: '50px',
})

const Content = styled.div({
  zIndex: 1000,
  width: '600px'
})

class Landing extends Component {
  state = {
    keyword: ''
  }

  updateKeyword = (keyword) => {
    this.setState({ keyword })
  }

  render() {
    return (
      <Wrapper>
        <Content>
          <SearchBoxContainer
            value={this.state.keyword}
            onChange={this.updateKeyword}
            onKeyPress={event => {
              if (event.charCode === 13) {
                history.push({ pathname: 'inventory', search: `item=${event.target.value}` })
              }
            }}
          />
        </Content>
        <Banner src={banner} />
      </Wrapper>
    )
  }
}

export default Landing
