import React, { Component } from 'react'
import styled from 'styled-components'
import { PoseGroup } from 'react-pose'

import { BumpPose } from '../poses'

import Icon from './Icon'

const alertMap = {
  error: { icon: 'error', background: '#ff3366' },
  warning: { icon: 'warning', background: '#fff35c' },
  success: { icon: 'check_circle', background: '#00ccff' },
}

const Wrapper = styled(BumpPose)(({ type, margin }) => ({
  display: 'flex',
  margin,
  fontSize: '12px',
  padding: '10px 8px',
  alignItems: 'center',
  background: alertMap[type].background,
  color: '#222',
  wordWrap: 'hidden'
}))

const Message = styled.div({
  display: 'flex',
})

class Alert extends Component {
  state = {
    visible: false
  }

  enter = () => {
    this.setState({ visible: true })
  }
  
  componentDidMount() {
    setTimeout(this.enter, 10)  // delay for entrance
  }

  componentWillUnmount() {
    clearTimeout(this.enter)
  }

  close = () => {
    setTimeout(this.props.close, 300) // delay for exit
    this.setState({ visible: false })
  }

  render() {
    const { alert: { type, message }, margin } = this.props
    const { visible } = this.state

    return (
      <PoseGroup>
        {visible && <Wrapper key='alert' type={type} margin={margin}>
          <Message>
            <Icon name={alertMap[type].icon} size='14px' color='#fafafa' margin='0px 10px 0 0' />
            {message}
          </Message>
        </Wrapper>}
      </PoseGroup>
    )
  }
}

export default Alert
