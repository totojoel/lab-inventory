import React, { Component } from 'react'
import styled from 'styled-components'
import moment from 'moment'

import Modal from './Modal'
import Icon from './Icon'
import Buttons from './Buttons'
import BarcodeReader from 'react-barcode-reader'
import Alert from './Alert'
import ShadowScrollbars from './ShadowScrollbars'

const Date = styled.div({
  marginTop: '-10px',
  '&:not(:last-child)': {
    marginBottom: '10px',
  },
  fontSize: '12px',
  color: '#999'
})

const ItemWrap = styled.div(({ highlighted, completed }) => ({
  background: highlighted ? '#00ccff' : '#fafafa',
  opacity: completed ? 1 : 0.8,
  padding: '10px',
  color: '#111',
  transition: '100ms',
  display: 'flex',
  fontSize: '14px',
  marginTop: '10px',
  marginBottom: '10px',
  alignItems: 'center',
}))

const CountWrap = styled.div({
  display: 'flex',
  justifyContent: 'flex-end',
  paddingRight: '4px',
  flex: 1
})

const Status = styled.div({
  textTransform: 'capitalize',
  fontSize: '14px',
})

const ListWrap = styled.div({
  overflow: 'auto',
  background: 'none',
})

class Item extends Component {
  state = {
    highlighted: false
  }

  componentDidUpdate(prevProps) {
    if (prevProps.scanned !== this.props.scanned) {
      this.setState({ highlighted: true })
      setTimeout(() => this.setState({ highlighted: false }), 500)
    }
  }

  status = () => {
    const nextStatus = {
      'pending': 'ready',
      'ready': 'receivable',
      'received': 'returned',
      'returned': 'returned'
    }
    if (this.props.scanned >= this.props.item.count) {
      return nextStatus[this.props.status]
    }
    return this.props.status
  }
  render() {
    const { item, scanned } = this.props
    return (
      <ItemWrap highlighted={this.state.highlighted} completed={item.count === scanned}>
        <div>{item.item.name} x {item.count}</div>
        <CountWrap>{scanned ? scanned : 0}</CountWrap>
        <Status>{this.status()}</Status>
      </ItemWrap>
    )
  }
}

const List = ({ items, status, scannedItems }) => {
  const scannable = status === 'pending' || status === 'received'
  return (
    <ListWrap>
      <ShadowScrollbars autoHeight={true} autoHeightMax={300} >
        {items && items.map((item, index) => {
          if (item.item) {
            return <Item
              status={status}
              key={index}
              item={item}
              scanned={scannable ? scannedItems.get(item.item.barcode) : item.count}
            />
          }
          return <ItemWrap key={index}>
            <Icon name='sentiment_very_dissatisfied' size='14px' color='#fafafa' margin='0 4px 1px 0' />
            Item is deleted.
          </ItemWrap>
        })}
      </ShadowScrollbars>
    </ListWrap>
  )
}

class Request extends Component {
  state = {
    visible: false,
    alert: null,
    scannedItems: new Map()
  }

  componentDidMount() {
    setTimeout(() => this.setState({ visible: true }), 10)  // delay for entrance
  }

  close = () => {
    setTimeout(this.props.close, 300) // delay for exit
    this.setState({ visible: false })
  }

  scan = (data) => {
    let scannedItems = this.state.scannedItems
    const scannedItem = this.props.request.items.find(item => item.item.barcode === data)
    if (scannedItem) {
      if (scannedItems.has(data) && scannedItem.count > scannedItems.get(data)) {
        scannedItems.set(data, 1 + scannedItems.get(data))
      }
      if (!scannedItems.has(data)) {
        scannedItems.set(data, 1)
      }
      this.setState({
        scannedItems
      })
    }
  }

  render() {
    const { request, advance, error } = this.props
    const nextStatus = {
      'pending': 'ready',
      'ready': 'received',
      'received': 'returned'
    }
    const { alert, scannedItems } = this.state
    const buttons = [
      { text: 'Close', primary: false, onClick: this.close }
    ]

    if (request && request.status !== 'returned') {
      buttons.push({
        text: 'Advance', primary: true, onClick: () => {
          if (request.status !== 'returned') {
            advance({ variables: { requestId: request.id, status: nextStatus[request.status] } })
              .then(this.close)
          }
        }
      })
    }

    return (
      <Modal
        visible={this.state.visible && !error}
        close={this.close}
        title={`Request by ${request && request.student ? request.student.firstName + ' ' + request.student.lastName : 'Deleted'}`}
        icon='description'
        renderContent={() => (
          request && <div>
            <BarcodeReader
              onError={(error) => this.setState({ error: error.message })}
              onScan={(data) => this.scan(data)}
            />
            {request.dateRequested && <Date>Added on {moment(request.dateRequested).format('LLL')}</Date>}
            {request.dateApproved && <Date>Approved on {moment(request.dateApproved).format('LLL')}</Date>}
            {request.dateReceived && <Date>Received on {moment(request.dateReceived).format('LLL')}</Date>}
            {request.dateReturned && <Date>Returned on {moment(request.dateReturned).format('LLL')}</Date>}
            {alert && <Alert alert={alert} />}
            {<List items={request.items} scannedItems={scannedItems} status={request.status} />}
            <Buttons buttons={buttons} />
          </div>
        )}
      />
    )
  }
}

export default Request
