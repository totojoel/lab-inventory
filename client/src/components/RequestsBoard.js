import React from 'react'
import styled from 'styled-components'

import RequestsListContainer from '../containers/RequestsListContainer'

const Wrapper = styled.div({
  display: 'flex',
  flexDirection: 'row',
  paddingBottom: '100px',
  height: '100%'
})

const RequestsBoard = ({ statuses }) => {
  return (
    <Wrapper>
      {statuses.map((status) => <RequestsListContainer key={status} status={status} />)}
    </Wrapper>
  )
}

export default RequestsBoard
