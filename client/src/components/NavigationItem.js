import React, { Component } from 'react'
import styled from 'styled-components'

import Icon from './Icon'

const Wrapper = styled.div(({ active, hovered, fixed, enabled }) => ({
  background: (active || hovered) ? '#00ccff' : '#111111',
  color: (active || hovered) ? '#222222' : '#fafafa',
  display: enabled ? 'flex' : 'none',
  height: '30px',
  minWidth: '30px',
  alignItems: 'center',
  fontSize: '14px',
  justifyContent: 'center',
  paddingLeft: (active || hovered || fixed) ? '8px' : 'unset',
  paddingRight: (active || hovered || fixed) ? '8px' : 'unset',
  marginLeft: '10px',
  transition: '300ms',
  cursor: 'pointer'
}))

class NavigationItem extends Component {
  state = {
    hovered: false
  }

  render() {
    const { text, icon, onClick, active, enabled, fixed } = this.props
    const { hovered } = this.state

    return (
      <Wrapper
        active={active} 
        fixed={fixed}
        onClick={onClick} 
        hovered={hovered} 
        enabled={enabled}
        onMouseEnter={() => this.setState({ hovered: true })}
        onMouseLeave={() => this.setState({ hovered: false })}
      >
        {icon && <Icon name={icon} size='16px' margin={(active || hovered || fixed) ? '0 4px 0 0' : '0'} />}
        {!fixed ? (active || hovered) && text : text}
      </Wrapper>
    )
  }
}

export default NavigationItem
