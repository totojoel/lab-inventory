import React from 'react'
import styled from 'styled-components'

import Button from './Button'

const Wrapper = styled.div({
  display: 'flex',
  marginTop: '20px',
  justifyContent: 'flex-end'
})

const Buttons = ({ buttons }) => (
  <Wrapper>{
    buttons.map(button => <Button key={button.text} {...button} />)
  }</Wrapper>
)

export default Buttons
