import React from 'react'
import styled from 'styled-components'
import posed, { PoseGroup } from 'react-pose'

import Icon from './Icon'

const FadePose = posed.div({
  enter: { opacity: 1, transition: { duration: 300, } },
  exit: { opacity: 0, transition: { duration: 300 } }
})

const Wrapper = styled(FadePose)({
  fontSize: '12px',
  position: 'absolute',
  top: 0,
  bottom: 0,
  right: 0,
  left: 0,
  color: '#fafafa',
  background: 'rgba(35, 35, 35, 0.6)',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
})

const Loading = ({ visible, message }) => (
  <PoseGroup>
    {visible && <Wrapper key='loading'>
      <Icon name='hourglass_empty' size='16px' margin={message ? '0 2px 0 0' : 'none'} />
      {message && message}
    </Wrapper>}
  </PoseGroup>
)

export default Loading
