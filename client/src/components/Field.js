import React, { Component } from 'react'
import styled from 'styled-components'

import Tip from './Tip'
import Icon from './Icon'

const Wrapper = styled.div({
  display: 'flex',
  flexDirection: 'column',
  position: 'relative',
})

const FieldWrap = styled.div({
  display: 'flex',
  width: '100%',
  background: '#191919',
  paddingTop: '2px',
  height: '30px',
  alignItems: 'center',
})

const Label = styled.div({
  whiteSpace: 'nowrap',
  color: '#00ccff',
  paddingLeft: '10px',
  paddingRight: '10px ',
  fontSize: '14px'
})

const Input = styled.input(({ type }) => ({
  flex: 1,
  width: type === 'number' ? '300px' : 'unset',
  overflow: 'hidden',
  paddingRight: '10px',
  background: 'none',
  border: 'none',
  color: '#fafafa'
}))

const ArrowsWrap = styled.div({
  position: 'absolute',
  right: 0,
  bottom: '6px',
  top: '8px',
  width: '20px',
  display: 'grid',
  alignItems: 'center',
  justifyItems: 'center',
  gridTemplateRows: '50% 50%'
})

const Arrows = () => (
  <ArrowsWrap>
    <Icon name='add' size='10px' margin='0' />
    <Icon name='remove' size='10px' margin='0' />
  </ArrowsWrap>
)

class Field extends Component {
  handleKey = (event) => {
    let currentValue = +event.target.value
    if (event.key === 'ArrowUp') {
      event.preventDefault()
      this.props.onChange(currentValue += 1)
    }
    if (event.key === 'ArrowDown') {
      event.preventDefault()
      this.props.onChange(currentValue -= 1)
    }
  }

  render() {
    const { label, value, error, onChange, type, placeholder } = this.props
    let inputProps = {
      placeholder,
      autoComplete: 'off',
      value,
      type,
      onChange: (event) => onChange(event.target.value)
    }

    if (type === 'count') {
      inputProps = {
        ...inputProps,
        onKeyDown: this.handleKey,
      }
    }

    return (
      <Wrapper>
        {type === 'count' && <Arrows />}
        <FieldWrap>
          <Label>{label}</Label>{}
          <Input {...inputProps} on/>
        </FieldWrap>
        <Tip visible={error} message={error ? error : ''} type='error' />
      </Wrapper >
    )
  }
}

export default Field
