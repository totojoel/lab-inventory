import React, { Component } from 'react'
import styled from 'styled-components'
import posed from 'react-pose'
import ReactToPrint from 'react-to-print'

import Icon from './Icon'
import Table from './Table'

const ExpandPose = posed.div({
  opened: {
    flex: 1
  },
  closed: {
    flex: 0
  }
})

const Wrapper = styled(ExpandPose)({
  marginLeft: '20px'
})

const Title = styled.div({
  height: '30px',
  whiteSpace: 'nowrap',
  color: '#00ccff',
  display: 'flex',
  flexDirection: 'row',
  fontFamily: 'VCR',
  fontSize: '16px',
  textTransform: 'uppercase',
  alignItems: 'center',
  borderBottom: 'solid 1px #494949',
  '&:hover': {
    cursor: 'pointer'
  }
})

const PrintButton = styled.div({
  marginLeft: '10px',
  color: '#999',
  '&:hover': {
    color: '#fafafa'
  }
})

class Report extends Component {
  state = {
    alert: null
  }

  render() {
    const { title, onClick, header, active, icon, rows } = this.props

    return <Wrapper pose={active ? 'opened' : 'closed'} ref={el => (this.componentRef = el)}>
      <Title onClick={onClick}><Icon name={icon} size='14px' margin='1px 6px 0 0' />{title}
        {active && <ReactToPrint
          trigger={() => <PrintButton><Icon name='print' size='14px' margin='1px 6px 0 0' />print</PrintButton>}
          content={() => this.componentRef}
        />}
      </Title>
      {active && <Table header={header} rows={rows} />}
    </Wrapper >
  }
}

export default Report
