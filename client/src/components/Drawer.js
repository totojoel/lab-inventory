import React from 'react'
import styled from 'styled-components'

import { MaximizePose } from '../poses'

import Icon from './Icon'

const Wrapper = styled.div({
  position: 'fixed',
  background: '#111',
  bottom: 0,
  right: '100px',
  zIndex: '1000',
  width: '300px',
})

const Title = styled.div({
  textTransform: 'uppercase',
  borderBottom: 'solid 1px #494949',
  fontSize: '16px',
  padding: '10px',
  background: '#111',
  color: '#00ccff',
  display: 'flex',
  alignItems: 'center',
  fontFamily: 'VCR',
  '&:hover': {
    cursor: 'pointer',
  }
})

const Content = styled(MaximizePose)({
  position: 'relative'
})

const ContentHolder = styled.div({
  position: 'absolute',
  top: '20px',
  bottom: '20px',
  right: '20px',
  left: '20px'
})

const IconHolder = styled.div({
  position: 'absolute',
  top: '14px',
  right: '10px',
  '&:hover': {
    color: '#fafafa',
    cursor: 'pointer'
  }
})

const Drawer = ({ open, icon, title, toggle, renderContent }) => {
  return (
    <Wrapper>
      <IconHolder onClick={toggle}>
        <Icon name={open ? 'minimize' : 'maximize'} size='16px' />
      </IconHolder>
      <Title onClick={toggle}><Icon name={icon} size='14px' margin='1px 6px 0 0' />{title}</Title>
      <Content pose={open ? 'opened' : 'closed'} open={open}>
        <ContentHolder>{
          renderContent()
        }</ContentHolder>
      </Content>
    </Wrapper>
  )
}

export default Drawer
