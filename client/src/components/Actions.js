import React from 'react'
import styled from 'styled-components'

import Action from './Action'

const Wrapper = styled.div({
  display: 'flex',
  justifyContent: 'flex-end',
})

const Actions = ({ actions }) => (
  <Wrapper>
    {actions.map(action => <Action key={action.text} {...action} />)}
  </Wrapper>
)

export default Actions
