import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div(({ primary, disabled }) => ({
  color: primary ? '#222' : '#00ccff',
  opacity: disabled ? 0.5 : 1,
  '&:hover': {
    color: disabled ? null : '#222',
    cursor: disabled ? 'not-allowed' : 'pointer',
    background: disabled ? null : '#00ccff',
  },
  transition: '300ms',
  border: primary ? 'none' : 'solid 1px #00ccff',
  background: primary ? '#00ccff' : 'none',
  paddingTop: '2px',
  paddingLeft: '10px',
  paddingRight: '10px',
  marginLeft: '10px',
  height: '30px',
  fontSize: '14px',
  display: 'flex',
  alignItems: 'center'
}))

const Button = ({ text, onClick, primary }) => (
  <Wrapper onClick={onClick} primary={primary}>{text}</Wrapper>
)

export default Button
