import React from 'react'
import styled from 'styled-components'
import { graphql, compose } from 'react-apollo'
import { gql } from 'apollo-boost'
import history from '../history'
import { withRouter } from 'react-router-dom'

import { TOGGLE_FORM, REQUEST_ITEM, UPDATE_FORM } from '../graphql/mutations'

import Item from './Item'
import Icon from './Icon'
import ShadowScrollbars from './ShadowScrollbars'

const AddButton = styled.div({
  height: '96px',
  opacity: 0.8,
  color: '#fafafa',
  width: '198px',
  float: 'left',
  display: 'flex',
  flexDirection: 'column',
  fontSize: '12px',
  alignItems: 'center',
  justifyContent: 'center',
  marginRight: '10px',
  background: 'none',
  '&:hover': {
    opacity: 1,
    cursor: 'pointer',
  },
  transition: '500ms'
})

const Wrapper = styled.div({
  position: 'absolute',
  width: '100%',
  height: '100%',
  // overflowY: 'auto'
})

const ItemsList = ({ items, request, isAdmin, toggleForm, updateForm, requestFormIsVisible }) => {
  return (
    <Wrapper>
      <ShadowScrollbars>
        {isAdmin && <AddButton key='1' onClick={() => toggleForm({ variables: { form: 'itemForm' } })} >
          <Icon name='add' size='20px' margin='0 0 4px 0' />
          <div>New item</div>
        </AddButton>}
        {items && items.map((item, index) => (
          <Item
            key={index}
            item={item}
            update={(item) => {
              const keys = ['id', 'name', 'barcode', 'count', 'defectiveCount']
              toggleForm({ variables: { form: 'itemForm' } })
              keys.forEach((key) => {
                updateForm({ variables: { form: 'itemForm', field: { [key]: item[key] } } })
              })
            }}
            discard={(item) => {
              history.push({
                pathname: '/inventory/delete-item/',
                search: `id=${item.id}&name=${item.name}`
              })
            }}
            request={(item) => {
              if (!requestFormIsVisible) {
                toggleForm({ variables: { form: 'requestForm' } })
              }
              request({ variables: { item } })
            }}
            isAdmin={isAdmin}
          />
        ))}
      </ShadowScrollbars>
    </Wrapper>
  )
}

export default compose(
  graphql(gql`{
    requestForm @client {
      visible
    }
  }`, {
      props: ({ data: { requestForm: { visible } } }) => ({
        requestFormIsVisible: visible
      })
    }
  ),
  graphql(UPDATE_FORM, { name: 'updateForm' }),
  graphql(TOGGLE_FORM, { name: 'toggleForm' }),
  graphql(REQUEST_ITEM, { name: 'request' }),
)(withRouter(ItemsList))