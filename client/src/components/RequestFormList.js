import React from 'react'
import styled from 'styled-components'
import { PoseGroup } from 'react-pose'
import { graphql, compose } from 'react-apollo'

import { BumpPose } from '../poses'

import { REQUEST_ITEM, DECREASE_ITEM, REMOVE_ITEM } from '../graphql/mutations'

import Actions from './Actions'
import ShadowScrollbars from './ShadowScrollbars'

const Wrapper = styled.div({
  position: 'relative',
  marginTop: '-5px',
  marginBottom: '-5px',
  height: '100%',
  width: '100%',
})

const List = styled.div({
  position: 'absolute',
  display: 'flex',
  height: '100%',
  width: '100%',
  overflowY: 'auto',
  overflowX: 'hidden',
  flexDirection: 'column',
})

const Item = styled.div({
  background: '#fafafa',
  padding: '6px 8px',
  color: '#111',
  display: 'flex',
  marginBottom: '10px',
  marginTop: '10px',
  opacity: 0.9
})

const Name = styled.div({
  display: 'flex',
  fontSize: '14px',
  alignItems: 'center',
  flex: 1
})

const RequestItem = ({ item, request, decrease, remove }) => {
  const { name, requestCount } = item
  const actions = [
    { text: 'Clear', icon: 'clear', onClick: remove, disableHover: true },
    { text: 'Remove', icon: 'remove', onClick: decrease, disableHover: true },
    { text: 'Add', icon: 'add', onClick: request, disableHover: true }
  ]

  return (
    <Item>
      <Name>{name} x {requestCount}</Name>
      <Actions actions={actions} />
    </Item>
  )
}

const RequestFormList = ({ items, request, decrease, remove }) => (
  <Wrapper>
    <List>
      <ShadowScrollbars>
        <PoseGroup>{
          items.map((item, index) => (
            <BumpPose key={index}>
              <RequestItem
                key={index}
                item={item}
                request={() => request({ variables: { item } })}
                decrease={() => decrease({ variables: { item: item.id } })}
                remove={() => remove({ variables: { item: item.id } })}
              />
            </BumpPose>
          ))
        }</PoseGroup>
      </ShadowScrollbars>
    </List>
  </Wrapper>
)

export default compose(
  graphql(REQUEST_ITEM, { name: 'request' }),
  graphql(DECREASE_ITEM, { name: 'decrease' }),
  graphql(REMOVE_ITEM, { name: 'remove' }),
)(RequestFormList)
