import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.i(({ size, margin }) => ({
  fontSize: size,
  margin
}))

const Icon = ({ name, size, margin }) => (
  <Wrapper className='material-icons' size={size} margin={margin}>{name}</Wrapper>
)

export default Icon
