import React from 'react'
import styled from 'styled-components'
import { PoseGroup } from 'react-pose'

import { BumpPose } from '../poses'

const Wrapper = styled(BumpPose)({
  display: 'flex',
  position: 'absolute',
  flexDirection: 'column',
  alignItems: 'center',
  width: '100%',
  top: '26px'
})

const Message = styled.div(({ type }) => ({
  background: type === 'error' ? '#ff3366' : '#fff35c',
  display: 'flex',
  color: '#222',
  fontSize: '12px',
  alignItems: 'center',
  textAlign: 'center',
  marginTop: '-6px',
  padding: '4px 6px'
}))

const Arrow = styled.div(({ type }) => ({
  height: '10px',
  width: '10px',
  transform: 'rotate(45deg)',
  background: type === 'error' ? '#ff3366' : '#fff35c',
  alignSelf: 'center'
}))


const Tip = ({ type, visible, message }) => (
  <PoseGroup>
    {visible && <Wrapper key='tip'>
      <Arrow type={type} /><Message type={type}>{message}</Message>
    </Wrapper>}
  </PoseGroup>
)

export default Tip
