import React from 'react'
import styled from 'styled-components'

import Icon from './Icon'

const Wrapper = styled.div({
  display: 'flex',
  height: '30px',
  width: '100%',
  background: '#191919',
  paddingTop: '2px',
  alignItems: 'center'
})

const Input = styled.input({
  flex: 1,
  overflow: 'hidden',
  paddingLeft: '10px',
  background: 'none',
  border: 'none',
})

const ClearIcon = styled.div({
  color: '#555',
  '&:hover': {
    color: '#fafafa',
    cursor: 'pointer'
  }
})

const SearchBox = ({ value, onChange, onKeyPress }) => (
  <Wrapper>
    <Icon name='search' size='16px' margin='0 0 0 10px' />
    <Input
      value={value ? value : ''}
      placeholder='Find an item'
      onChange={event => onChange(event.target.value)}
      onKeyPress={onKeyPress}
    />
    <ClearIcon onClick={() => onChange('')}>
      <Icon name='clear' size='14px' margin='4px 10px 0 0' />
    </ClearIcon>
  </Wrapper>
)

export default SearchBox
