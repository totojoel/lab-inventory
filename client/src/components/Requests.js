import React from 'react'
import styled from 'styled-components'

import RequestsContainer from '../containers/RequestsContainer'

const Wrapper = styled.div({
  color: 'white',
  display: 'grid',
  gridTemplateColumns: 'auto auto auto',
  height: '100%',
})

const Requests = () => (
  <Wrapper>
    <RequestsContainer status='pending' />
    <RequestsContainer status='approved' />
    <RequestsContainer status='returned' />
  </Wrapper>
)

export default Requests
