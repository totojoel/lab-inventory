import React, { Component } from 'react'
import styled from 'styled-components'
import { PoseGroup } from 'react-pose'

import { BumpPose } from '../poses'

import FieldContainer from '../containers/FieldContainer'

const Wrapper = styled.div({
  width: '100%',
  alignSelf: 'center',
  position: 'relative'
})

const FormItem = styled(BumpPose)(({ columnCount }) => ({
  position: 'relative',
  columnCount: columnCount,
  columnGap: '10px',
  '&:not(:last-child)': {
    marginBottom: '10px'
  }
}))

class Form extends Component {
  componentWillMount() {
    this.keyListener = (event) => {
      if (event.code === 'Enter') {
        this.props.submit()
      }
    }
    window.addEventListener('keydown', this.keyListener)
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.keyListener)
  }

  render() {
    const { items, values, update } = this.props
    return (
      <Wrapper>
        <PoseGroup>
          {
            items.map(({ key, fields }) =>
              <FormItem key={key} columnCount={fields.length}>{
                fields.map((field) =>
                  <FieldContainer
                    onChange={update}
                    key={field.id}
                    shouldBlur={field.shouldBlur}
                    value={values[field.id]}
                    {...field}
                  />
                )}
              </FormItem>
            )
          }
        </PoseGroup>
      </Wrapper>
    )
  }
}

export default Form
