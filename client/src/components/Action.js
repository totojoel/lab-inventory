import React, { Component } from 'react'
import styled from 'styled-components'

import Icon from './Icon'
import Tip from './Tip'

const Wrapper = styled.div({
  color: '#999',
  borderRadius: '10px',
  width: '20px',
  height: '20px',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  '&:hover': {
    background: '#fff35c',
    color: '#111',
    cursor: 'pointer'
  },
  position: 'relative',
  transition: '300ms'
})

class Action extends Component {
  state = {
    hovered: false
  }

  render() {
    const { text, icon, onClick, disableHover } = this.props
    const { hovered } = this.state

    return (
      <Wrapper
        onClick={onClick}
        onMouseEnter={() => !disableHover && this.setState({ hovered: true })}
        onMouseLeave={() => !disableHover && this.setState({ hovered: false })}
      >
        <Icon name={icon} size='14px' />
        {text && <Tip visible={hovered} message={text} />}
      </Wrapper>
    )
  }
}


export default Action
