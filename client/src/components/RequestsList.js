import React from 'react'
import styled from 'styled-components'

import RequestPreview from './RequestPreview'
import ShadowScrollbars from './ShadowScrollbars'

const Wrapper = styled.div({
  flex: 1,
  display: 'flex',
  flexDirection: 'column',
  '&:not(:last-child)': {
    marginRight: '40px'
  },
})

const List = styled.div({
  overflowY: 'auto',
  overflowX: 'hidden',
  width: '100%',
  height: '100%',
  position: 'relative',
})

const Title = styled.div({
  minHeight: '30px',
  whiteSpace: 'nowrap',
  color: '#00ccff',
  display: 'flex',
  fontFamily: 'VCR',
  fontSize: '16px',
  textTransform: 'uppercase',
  alignItems: 'center',
  borderBottom: 'solid 1px #494949',
})

const EmptyWrapper = styled.div({
  display: 'flex',
  margin: 'auto',
  opacity: 0.5,
  fontSize: '12px'
})

const RequestsList = ({ name, requests }) => {
  return (
    <Wrapper>
      <Title>{name} {requests ? requests.length : 0}</Title>
      {requests && requests.length > 0 ?
        <List>
          <ShadowScrollbars>
            {requests.map((request) => (
              <RequestPreview key={request.id} request={request} />
            ))}
          </ShadowScrollbars>
        </List>
        : <EmptyWrapper>Empty</EmptyWrapper>
      }
    </Wrapper>
  )
}

export default RequestsList
