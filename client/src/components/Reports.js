import React, { Component } from 'react'
import styled from 'styled-components'
import moment from 'moment'

import ItemsReportContainer from '../containers/ItemsReportContainer'
import RequestsReportContainer from '../containers/RequestsReportContainer'

const DateInput = styled.input(({ highlight }) => ({
  height: '30px',
  padding: 0,
  border: 'none',
  color: highlight ? '#222' : '#fafafa',
  paddingLeft: '10px',
  background: highlight ? '#00ccff' : 'none',
  '&:hover': {
    color: '#00ccff',
    background: 'none',
    cursor: 'pointer'
  },
  '&::placeholder': {
    color: '#fafafa'
  },
  fontFamily: 'VCR',
  textTransform: 'uppercase',
  fontSize: '16px',
  transition: '300ms'
}))

const Wrapper = styled.div({
  display: 'flex',
  position: 'absolute',
  flexDirection: 'row',
  width: '100%',
  height: '100%'
})

const Buttons = styled.div({
  display: 'flex',
  flexDirection: 'column',
  width: '250px'
})

const Button = styled.div(({ highlight }) => ({
  height: '30px',
  whiteSpace: 'nowrap',
  color: highlight ? '#222' : '#fafafa',
  paddingLeft: '10px',
  background: highlight ? '#00ccff' : 'none',
  display: 'flex',
  fontFamily: 'VCR',
  fontSize: '16px',
  textTransform: 'uppercase',
  alignItems: 'center',
  '&:hover': {
    color: '#00ccff',
    background: 'none',
    cursor: 'pointer'
  },
  transition: '300ms'
}))

const Shadow = styled.div({
  background: 'linear-gradient(transparent, #222)',
  position: 'fixed',
  height: '100px',
  bottom: 0,
  right: 0,
  left: 0,
})

const Main = styled.div({
  display: 'flex',
  flexDirection: 'row',
  width: '100%'
})

class Reports extends Component {
  state = {
    active: 'today',
    from: moment(),
    to: null
  }

  all = () => {
    this.setState({
      active: 'all', from: null, to: null
    })
  }

  today = () => {
    this.setState({
      active: 'today',
      from: moment(),
      to: null
    })
  }

  from = (date) => {
    this.setState({
      active: null,
      from: moment(date),
    })
  }

  to = (date) => {
    this.setState({
      active: null,
      to: moment(date)
    })
  }

  render() {
    const { from, to, active } = this.state
    let variables = {}

    if (from && !to) {
      variables = {
        from: +from.startOf('day').format('x'),
        to: +from.endOf('day').format('x')
      }
    }

    if (to && !from) {
      variables = {
        from: +to.startOf('day').format('x'),
        to: +to.endOf('day').format('x')
      }
    }

    if (to && from) {
      variables = {
        from: +from.startOf('day').format('x'),
        to: +to.endOf('day').format('x')
      }
    }

    if (variables.from > variables.to) {
      variables = {
        from: variables.to,
        to: variables.from
      }
    }

    return <Wrapper>
      <Buttons>
        <Button highlight={active === 'all'} onClick={this.all}>Show All</Button>
        <Button highlight={active === 'today'} onClick={this.today}>Show Today</Button>
        <DateInput
          type='text'
          highlight={from}
          onClick={(event) => {
            event.target.type = 'date'
          }}
          onBlur={(event) => {
            event.target.type = 'text'
          }}
          placeholder='Set Starting Date'
          value={from ? from.format('ll') : ''}
          onChange={(event) => {
            this.from(event.target.value)
            event.target.type = 'text'
          }}
        />
        <DateInput
          type='text'
          highlight={to}
          onClick={(event) => {
            event.target.type = 'date'
          }}
          placeholder='Set Ending Date'
          onBlur={(event) => {
            event.target.type = 'text'
          }}
          value={to ? to.format('ll') : ''}
          onChange={(event) => {
            this.to(event.target.value)
            event.target.type = 'text'
          }}
        />
      </Buttons>
      <Main>
        <RequestsReportContainer variables={variables} />
        <ItemsReportContainer variables={variables} />
      </Main>
      <Shadow />
    </Wrapper>
  }
}

export default Reports