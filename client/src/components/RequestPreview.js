import React, { Component } from 'react'
import styled from 'styled-components'
import moment from 'moment'
import history from '../history'
import { PoseGroup } from 'react-pose'

import { BumpPose } from '../poses'

const SmallText = styled.div({
  fontSize: '12px'
})

const Grey = styled(SmallText)({
  color: '#696969',
  fontSize: '12px'
})

const Wrapper = styled.div({
  background: '#fafafa',
  marginTop: '10px',
  marginBottom: '10px',
  opacity: 0.9,
  maxWidth: '200px',
  minWidth: '200px',
  padding: '20px 18px',
  color: '#222',
  fontSize: '14px',
  '&:hover': {
    cursor: 'pointer',
    opacity: 1
  }
})

class RequestPreview extends Component {
  state = {
    time: moment(this.props.request.dateRequested).fromNow(),
    visible: false
  }

  timer = () => {
    this.setState({
      time: moment(this.props.request.dateRequested).fromNow()
    })
  }

  componentDidMount() {
    setTimeout(() => this.setState({ visible: true }), 10)  // delay for entrance
  }

  componentWillMount() {
    this.timerInterval = setInterval(this.timer, 1000)
  }

  componentWillUnmount() {
    clearInterval(this.timerInterval)
    this.timerInterval = null
  }

  render() {
    const { request: { id, student, items } } = this.props
    let name = 'Deleted'

    if (student) {
      const { firstName, lastName } = student
      name = firstName + ' ' + lastName
    }
	
    let count = 0
    items.forEach((item) => {
      count += item.count
    })

    return (
      <PoseGroup>{
        this.state.visible && <BumpPose key='1'>
          <Wrapper onClick={() => history.push({ pathname: '/requests/' + id })} >
            {name}
            <SmallText>{count} {`item${count > 1 ? 's' : ''}`}</SmallText>
            <Grey>{this.state.time}</Grey>
          </Wrapper>
        </BumpPose>
      }</PoseGroup>
    )
  }
}

export default RequestPreview
