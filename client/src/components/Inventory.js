import React from 'react'
import styled from 'styled-components'
import history from '../history'

import ItemList from './ItemsList'
import Alert from './Alert'
import SearchBoxContainer from '../containers/SearchBoxContainer'
import RequestFormContainer from '../containers/RequestFormContainer'

const Wrapper = styled.div({
  display: 'flex',
  flexDirection: 'column',
  height: '100%'
})

const Title = styled.div({
  height: '30px',
  width: 'auto',
  whiteSpace: 'nowrap',
  marginRight: '10px',
  color: '#00ccff',
  display: 'flex',
  fontFamily: 'VCR',
  fontSize: '16px',
  textTransform: 'uppercase',
  alignItems: 'center',
  borderBottom: 'solid 1px #494949'
})

const TopBar = styled.div({
  display: 'flex',
  marginBottom: '20px'
})

const SearchBoxWrap = styled.div({
  width: '300px'
})

const Results = styled.div({
  position: 'relative',
  height: '100%',
})

const Inventory = ({ items, alert, user }) => {
  let isAdmin = user ? user.isAdmin : false
  return (
    <Wrapper>
      <RequestFormContainer user={user} />
      <TopBar>
        <Title>Found {items ? items.length : 0} items</Title>
        <SearchBoxWrap>
          <SearchBoxContainer
            onChange={keyword => history.push({ pathname: 'inventory', search: `item=${keyword}` })}
          />
        </SearchBoxWrap>
      </TopBar>
      <Results>
        {alert && <Alert alert={alert} margin='0 0 20px 0' />}
        <ItemList items={items} isAdmin={isAdmin} />
      </Results>
    </Wrapper>
  )
}

export default Inventory
