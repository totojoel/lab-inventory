import React, { Component } from 'react'
import styled from 'styled-components'
import { PoseGroup } from 'react-pose'
import history from '../history'

import { BumpPose } from '../poses'

const Wrapper = styled.div({
  display: 'flex',
  flexDirection: 'column',
  position: 'relative',
  width: '100%',
  height: '100%',
})

const RowsWrap = styled.tbody({
  position: 'absolute',
  overflowY: 'auto',
  width: '100%',
  top: '40px',
  bottom: '40px',
})

const Row = styled.tr({
  display: 'flex',
  background: '#fafafa',
  alignItems: 'center',
  '&:nth-child(even)': {
    opacity: 0.8,
  }
})

const TitleRow = styled(Row)({
  background: '#222',
  margin: '0px',
  position: 'absolute',
  height: '40px',
  left: 0,
  right: 0,
})

const cellStyle = {
  padding: '10px',
  display: 'flex',
  color: '#111',
  fontSize: '12px',
  flex: 1,
  '&:first-child': {
    maxWidth: '50px'
  }
}

const TitleCellWrap = styled.th(({ active }) => ({
  ...cellStyle,
  fontWeight: 'normal',
  textDecoration: active ? 'underline' : 'none',
  color: '#00ccff',
  fontSize: '14px',
  '&:hover': {
    cursor: 'pointer'
  }
}))

const Cell = styled.td(cellStyle)

class TitleCell extends Component {
  state = {
    active: false
  }

  handleClick = () => {
    if (this.props.title !== '#') {
      history.push({ pathname: history.location.pathname, search: `sort=${this.props.index}` })
    }
  }

  render() {
    const params = new URLSearchParams(history.location.search)
    const sort = params.get('sort')
    return (
      <TitleCellWrap
        active={this.state.active || sort === this.props.index}
        onMouseEnter={() => this.setState({ active: true })}
        onMouseLeave={() => this.setState({ active: false })}
        onClick={this.handleClick}
      >
        {this.props.title}
      </TitleCellWrap>
    )
  }
}

class Table extends Component {
  state = {
    visible: false
  }

  componentDidMount() {
    setTimeout(() => this.setState({ visible: true }), 0)
  }

  render() {
    const { header, rows } = this.props

    return <Wrapper><PoseGroup>{this.state.visible && rows && <BumpPose key='1'><table>
      {header &&
        <thead>
          <TitleRow>
            <TitleCellWrap>#</TitleCellWrap>
            {header.map((title, index) => (
              <TitleCell key={index} index={index} title={title} />
            ))}
          </TitleRow>
        </thead>
      }
      {rows &&
        <RowsWrap>
          {
            rows.map((row, index) => (
              <Row key={index}>
                <Cell>{index + 1}</Cell>
                {row.map((cell, index) => <Cell key={index}>{cell}</Cell>)}
              </Row>
            ))
          }
        </RowsWrap>
      }
      {!rows.length && <RowsWrap><Row><Cell>Empty</Cell></Row></RowsWrap>}
    </table></BumpPose>}</PoseGroup></Wrapper>
  }
}

export default Table
