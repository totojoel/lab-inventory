import css from 'dom-css'
import React, { Component } from 'react'
import { Scrollbars } from 'react-custom-scrollbars'

class ShadowScrollbars extends Component {
  constructor(props, ...rest) {
    super(props, ...rest)
    this.state = {
      scrollTop: 0,
      scrollHeight: 0,
      clientHeight: 0
    }
  }

  renderThumb = ({ style, ...props }) => {
    const thumbStyle = {
      background: '#696969',
    }
    return (
      <div style={{ ...style, ...thumbStyle }} {...props} />
    )
  }

  handleUpdate = (values) => {
    const { shadowTop, shadowBottom } = this.refs
    const { scrollTop, scrollHeight, clientHeight } = values
    const shadowTopOpacity = 1 / 20 * Math.min(scrollTop, 20)
    const bottomScrollTop = scrollHeight - clientHeight
    const shadowBottomOpacity = 1 / 20 * (bottomScrollTop - Math.max(scrollTop, bottomScrollTop - 20))
    css(shadowTop, { opacity: shadowTopOpacity })
    css(shadowBottom, { opacity: shadowBottomOpacity })
  }

  render() {
    const { style, ...props } = this.props
    const containerStyle = {
      ...style,
      position: 'relative',
      height: 'inherit',
      maxHeight: 'inherit'
    }
    const shadowTopStyle = {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      height: 20,
      background: 'linear-gradient(to bottom, rgba(0, 0, 0, 0.3) 0%, rgba(0, 0, 0, 0) 100%)'
    }
    const shadowBottomStyle = {
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
      height: 20,
      background: 'linear-gradient(to top, rgba(0, 0, 0, 0.3) 0%, rgba(0, 0, 0, 0) 100%)'
    }

    return (
      <div style={containerStyle}>
        <Scrollbars
          ref='scrollbars'
          onUpdate={this.handleUpdate}
          renderThumbHorizontal={this.renderThumb}
          renderThumbVertical={this.renderThumb}
          autoHide={true}
          {...props} />
        <div
          ref='shadowTop'
          style={shadowTopStyle} />
        <div
          ref='shadowBottom'
          style={shadowBottomStyle} />
      </div>
    )
  }
}

export default ShadowScrollbars
