import React, { Component } from 'react'
import styled from 'styled-components'
import posed, { PoseGroup } from 'react-pose'

import Icon from './Icon'

const PosedMask = posed.div({
  enter: { opacity: 1, transition: { duration: 300, } },
  exit: { opacity: 0, transition: { duration: 300 } }
})

const PosedWrap = posed.div({
  enter: {
    y: 0, transition: {
      y: { type: 'spring', stiffness: 400, damping: 15 },
    }
  },
  exit: {
    y: '-25px', transition: {
      y: { type: 'spring', stiffness: 400, damping: 15 },
    }
  }
})

const Mask = styled(PosedMask)({
  display: 'grid',
  position: 'absolute',
  height: '100%',
  width: '100%',
  zIndex: '2000',
  background: 'rgba(0, 0, 0, 0.6)',
  alignItems: 'center',
  justifyItems: 'center',
})

const Wrapper = styled(PosedWrap)({
  background: '#222',
  width: '400px',
  position: 'relative'
})

const Title = styled.div({
  textTransform: 'uppercase',
  fontSize: '16px',
  padding: '20px',
  paddingBottom: 0,
  color: '#00ccff',
  display: 'flex',
  alignItems: 'center',
  fontFamily: 'VCR'
})

const Content = styled.div({
  padding: '20px'
})

const IconHolder = styled.div({
  position: 'absolute',
  top: '10px',
  right: '10px',
  color: '#555',
  '&:hover': {
    color: '#fafafa',
    cursor: 'pointer'
  }
})

class Modal extends Component {
  componentWillMount() {
    this.keyListener = (event) => {
      if (event.code === 'Escape') {
        if (this.props.visible) {
          this.props.close()
        }
      }
    }
    window.addEventListener('keydown', this.keyListener)
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.keyListener)
  }

  render() {
    const { visible, close, icon, title, renderContent } = this.props
    return (
      <PoseGroup>
        {visible &&
          <Mask key='1'>
            <Wrapper>
              <IconHolder onClick={close}><Icon name='close' size='16px' /></IconHolder>
              <Title><Icon name={icon} size='14px' margin='1px 6px 0 0' />{title}</Title>
              <Content>{renderContent()}</Content>
            </Wrapper>
          </Mask>
        }
      </PoseGroup>
    )
  }
}

export default Modal
