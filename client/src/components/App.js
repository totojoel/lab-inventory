import React from 'react'
import styled from 'styled-components'
import { Router, Route, Redirect } from 'react-router-dom'
import history from '../history'

import Landing from './Landing'
import ConfirmContainer from '../containers/ConfirmContainer'
import NavigationContainer from '../containers/NavigationContainer'
import SignInFormContainer from '../containers/SignInFormContainer'
import ItemFormContainer from '../containers/ItemFormContainer'
import InventoryContainer from '../containers/InventoryContainer'
import RequestsBoardContainer from '../containers/RequestsBoardContainer'
import RequestContainer from '../containers/RequestContainer'
import ReportsContainer from '../containers/ReportsContainer'
import NotificationsContainer from '../containers/NoficationsContainer'

const Wrapper = styled.div({
  position: 'absolute',
  width: '100%',
  height: '100%',
})

const Content = styled.div({
  position: 'absolute',
  top: '100px',
  right: '100px',
  left: '100px',
  bottom: '0',
  overflow: 'hidden'
})

const FooterWrap = styled.div({
  position: 'fixed',
  bottom: 0,
  right: 0,
  color: '#999',
  fontSize: '10px',
  padding: '10px',
  '&:hover': {
    color: '#fafafa',
    cursor: 'pointer'
  }
})

const Footer = () => (
  <FooterWrap onClick={() => window.location.assign('https:/gitlab.com/totojoel')}>totojoel</FooterWrap>
)

const App = ({ user }) => (
  <Router history={history}>
    <Wrapper>
      <NotificationsContainer />
      <SignInFormContainer />
      <ItemFormContainer />
      <NavigationContainer user={user} />
      <Route exact path='/' component={Landing} />
      <Route path='/requests/:id' component={RequestContainer} />
      <Route path='*/delete-item' component={ConfirmContainer} />
      <Content>
        <Route path='/inventory' render={() => <InventoryContainer user={user} />} />
        <Route path='/requests' render={() => <RequestsBoardContainer user={user} />} />
        <Route exact path='/reports' render={() => <Redirect to='/reports/items' />} />
        <Route path='/reports/:doc' render={() => <ReportsContainer user={user} />} />
      </Content>
      <Footer />
    </Wrapper>
  </Router>
)

export default App
