export const validateStudentNumber = (value) => {
  const pattern = /[0-9]{2}-[0-9]{4}-[0-9]{2}/g
  const match = pattern.test(value)
  
  return match ? null : 'Your student number is invalid.'
}

export const validatePassword = (value) => {
  const pattern = /.{8}/g
  const match = pattern.test(value)
  
  return match ? null : 'Your password must be at least 8 characters.'
}

export const validateName = (value) => {
  const pattern = /\d|\s{2,}|[!1@#$%^&*()_+{}|":?><~`';/,[\]]/g
  const match = pattern.test(value)
  
  return match ? 'Your name is invalid.' : null 
}

export const validateItem = (value) => {
  const pattern = /\s{2,}|[!1@#$%^&*()_+{}|":?><~`';/,[\]]/g
  const match = pattern.test(value)
  
  return match ? 'Your item is invalid.' : null 
}

export const validateUnits = (value) => {  
  return value > 1 ? null : 'Units must be more than 1.' 
}
