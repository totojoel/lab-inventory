export const signIn = (data) => {
  localStorage.setItem('token', data.token)
}

export const signOut = () => {
  window.location.reload()
  localStorage.clear('token')
}
